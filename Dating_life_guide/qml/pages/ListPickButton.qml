import QtQuick 2.0
import Felgo 3.0

Rectangle {
    id: listPickButton
    width: 250
    height: 30

    property string defaultColor: "white"
    property string pressColor: "grey"
    property alias textOnButton: buttonText.text

    signal clicked

    border.width: 2

    opacity: buttonArea.containsMouse ? 0.8:1
    color: buttonArea.pressed ? pressColor:defaultColor

    AppText {
        id: buttonText
        anchors.centerIn: parent
        color: "black"
    }

    MouseArea {
        id: buttonArea
        hoverEnabled: true
        anchors.fill: listPickButton
        onClicked: listPickButton.clicked()
    }
}
