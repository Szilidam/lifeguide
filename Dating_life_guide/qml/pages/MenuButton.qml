import QtQuick 2.0
import Felgo 3.0

/*

// EXAMPLE USAGE:
// add this piece of code in your Scene to display the Button

MenuButton {
  text: "Click Me!"
  width: 80
  height: 40
  anchors.centerIn: parent

  onClicked: {
    console.log("Clicked!")
  }
}

*/
/*
Item {
    id: button

    // public events
    signal clicked
    signal pressed
    signal released

    property string defaultColor: "red"
    property string hoverColor: "blue"
    property alias text: buttonText.text

    // button background
    Rectangle {
        id: background
        //color: mouseArea.containsMouse ? hoverColor : defaultColor
        color: defaultColor
        anchors.fill: parent
        //width: 100
        //height: 50
    }

    // button text
    Text {
        id: buttonText
        anchors.centerIn: background
    }

    // mouse area to handle click events
    MouseArea {
        id: mouseArea
        anchors.fill: background
        hoverEnabled: true

        onPressed: button.pressed()
        onReleased: button.released()
        onClicked: button.clicked()
    }



    // change opacity on pressed and released events, so we have a "pressed" state
    onPressed: {
        opacity = 0.5
    }

    onReleased: {
        opacity = 1.0
    }
}
*/

Rectangle {
    id: menuButton
    width: 200
    height: 50

    property string defaultColor: "green"
    property string pressColor: "blue"
    property alias textOnButton: buttonText.text

    signal clicked


    radius: 4
    border.width: 2

    opacity: buttonArea.containsMouse ? 0.8:1
    color: buttonArea.pressed ? pressColor:defaultColor

    AppText {
        id: buttonText
        anchors.centerIn: parent
        color: "white"
    }

    MouseArea {
        id: buttonArea
        hoverEnabled: true
        anchors.fill: menuButton
        onClicked: menuButton.clicked()
    }
}


