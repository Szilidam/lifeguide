import QtQuick 2.0
import Felgo 3.0

Item {

    id: databaseHandler

    function getDBValue (DB_id,key,Indicator_id) {
        var approachLine=DB_id.getValue(key)
        if (approachLine===undefined){
            Indicator_id.text="No advice to give!"
            Indicator_id.color="red"
        }
        else{
            //openingLine.text=approachLine
            try {
                //Indicator_id.text=JSON.parse(approachLine).adviceText
                var adviceGroup=JSON.parse(approachLine)
                var chosenLine=Math.floor(Math.random()*adviceGroup.advices.length)
                Indicator_id.text=adviceGroup.advices[chosenLine].adviceText
                Indicator_id.color="black"
            }
            catch (error) {
                //If we can not parse the object, we do something with the error to handle it
                Indicator_id.text="No advice to give!"
                //openingLine.text=error
                Indicator_id.color="red"
            }
        }

    }

    function setDBValue (DB_id,key,Indicator_id,MSGB_id) {
        var adviceGroup
        try {
            adviceGroup=JSON.parse(DB_id.getValue(key))
        }
        catch(error) {
            adviceGroup=undefined
        }
        if (adviceGroup===undefined) {
            adviceGroup={advices:[]}
        }
        var textToSave=Indicator_id.text
        var adviceList=[]
        for(var i=0;i<adviceGroup.advices.length;i++) {
            adviceList.push(adviceGroup.advices[i].adviceText)
        }
        var setSuccessful=false
        if(!adviceList.includes(textToSave) && textToSave!=="") {
            var advice= {adviceText:""}
            advice.adviceText=textToSave
            adviceGroup.advices.push(advice)
            var advices=JSON.stringify(adviceGroup)
            DB_id.setValue(key,advices)
            setSuccessful=true
        }
        else{
            MSGB_id.title="This element is already in the list!"
            MSGB_id.visible=true
            MSGB_id.open()
        }

        return setSuccessful
        //Indicator_id.text=""

        /*
        var advice= {adviceText:""}
        advice.adviceText=textToSave
        var advices=JSON.stringify(advice)
        DB_id.setValue(key,advices)
        */
    }

    function listDBValues (DB_id,key) {
        var adviceGroup
        try {
            adviceGroup=JSON.parse(DB_id.getValue(key))
        }
        catch(error) {
            adviceGroup=undefined
        }
        if (adviceGroup===undefined) {
            adviceGroup={advices:[]}
        }
        var adviceList=[]
        for(var i=0;i<adviceGroup.advices.length;i++) {
            adviceList.push(adviceGroup.advices[i].adviceText)
        }
        return adviceList
    }
    function removeDBValueByAdviceText(DB_id,key,AdviceText) {
        var adviceGroup
        try {
            adviceGroup=JSON.parse(DB_id.getValue(key))
        }
        catch(error) {
            adviceGroup=undefined
        }
        if (adviceGroup===undefined) {
            adviceGroup={advices:[]}
        }
        var removeIndex=-1
        for(var i=0;i<adviceGroup.advices.length;i++) {
            if(adviceGroup.advices[i].adviceText===AdviceText) {
                removeIndex=i
            }
        }
        if(removeIndex>=0) {
            adviceGroup.advices.splice(removeIndex,1)
        }
        var advices=JSON.stringify(adviceGroup)
        DB_id.setValue(key,advices)
}
    function displayList(model_id,db_id,key) {
        var lineList=listDBValues(db_id,key)
        for (var i=0;i<lineList.length;i++){
            model_id.append({name: lineList[i]})
        }
    }

}
