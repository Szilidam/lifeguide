import QtQuick 2.0
import Felgo 3.0

Page {
    id: db_handler
    visible: false

    property string listKey: "Lists of faith" //This key leads to the list of the lists. As further development this must be read from database

    title: "Database Management"

    onAppeared: {
        db_op.displayList(adviceList,database,db_handler.listKey)
        listPicker.textOnButton=db_handler.listKey
    }

    AppImage {
        id: appImage
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        source: "../../assets/Note.jpg"
    }

    Database {
        id: database
    }

    DatabaseOperation {
        id: db_op
    }

    Column {

        spacing: 10
        anchors.centerIn: parent

        ListPickButton {
            id: listPicker
            textOnButton: "Choose your List"
            onClicked: {
                pickList.append({name: db_handler.listKey})
                db_op.displayList(pickList,database,db_handler.listKey)
                listPickDialog.visible=true
                listPickDialog.open()
            }

        }
/*
        Dialog {
            id: listdialog
            width: 300
            height: 188
            autoSize: true
            isOpen: false
            positiveAction: true
            positiveActionLabel: "Add"
            negativeAction: false
            title: "Enter the name of the new list"
            visible: false
            onAccepted: {
                visible=false
                close()
            }
            AppTextField {
                id: dialogTextField
                width: parent.width*0.8
                //height: 50
                backgroundColor: "grey"
                wrapMode: "WordWrap"
                radius: 4
                x: 5
            }
        }*/

        Dialog {
            id: listPickDialog
            width: 300
            height: 188
            autoSize: true
            isOpen: false
            positiveAction: false
            negativeAction: false
            title: "Choose the list to edit"
            visible: false
            onClosed: {
                pickList.clear()
            }

            ListView {
                id: listPickList
                width: 280
                height: 140
                model: ListModel {
                    id: pickList
                }
                delegate: ListPickButton {
                    textOnButton: name
                    onClicked: {
                        var currentItem=pickList.get(index)
                        listPicker.textOnButton=currentItem.name
                        adviceList.clear()
                        db_op.displayList(adviceList,database,currentItem.name)
                        listPickDialog.visible=false
                        listPickDialog.close()
                    }
                }
            }
        }


        ListView {
            id: listView
            width: 360
            height: 160
            model: ListModel {
                id: adviceList
            }
            delegate: Rectangle {
                width: 360
                height: 40
                color: listPicker.textOnButton==db_handler.listKey ? "yellow" : "white"
                //color: "white"
                //color: ListView.isCurrentItem ? "white" : "red"
                Row {
                    id: row1
                    width: parent.width
                    height: parent.height
                    x: 5
                    y: 5

                    DeleteButton {
                        id: removal
                        onClicked: {
                            if (listPicker.textOnButton===listKey) {
                             database.clearValue(adviceList.get(index).name) //clear the given value to free memory if the list of lists is handled
                            }
                            db_op.removeDBValueByAdviceText(database,listPicker.textOnButton,adviceText.text) //Remove text from database
                            adviceList.remove(index) //Remove text from list
                            //the index variable can be used to get the index of the current element
                        }
                    }

                    Text {
                        id: adviceText
                        text: name
                        font.bold: true
                        anchors.verticalCenter: parent.verticalCenter
                        width: parent.width-row1.spacing-removal.width
                        wrapMode: Text.WordWrap
                    }

                    spacing: 10
                }
            }
        }


        AppTextField {
            id: appTextField
            width: 360
            height: 300
            backgroundColor: "white"
            wrapMode: "WordWrap"
        }

        MenuButton {
            textOnButton: "Save"
            onClicked: {
                //database.clearAll()
                if(!(listPicker.textOnButton===db_handler.listKey&&listPicker.textOnButton===appTextField.text)) {
                var setOK=db_op.setDBValue(database,listPicker.textOnButton,appTextField,dialog) //Add text to database
                if (setOK) {
                    adviceList.append({name: appTextField.text}) //Add text to list
                }
                appTextField.text=""
                }
            }

        }

        Dialog {
            id: dialog
            //anchors.centerIn: parent
            width: 300
            height: 188
            autoSize: true
            isOpen: false
            positiveAction: true
            positiveActionLabel: "OK"
            negativeAction: false
            title: ""
            visible: false
            onAccepted: {
                visible=false
                close()
            }
        }


    }
}

/*##^##
Designer {
    D{i:1;anchors_height:200}
}
##^##*/
