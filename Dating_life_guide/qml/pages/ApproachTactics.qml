import QtQuick 2.0
import Felgo 3.0

Page {
    id: approachTactics

    property string listKey: "Lists of faith" //This key leads to the list of the lists. As further development this must be read from database
    property bool listChosen: false


    title: "Approach Tactics"

    /*
    AppImage {
        id: appImage
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        source: "../../assets/Park.jpg"
    }
    */

    Database {
        id: database
    }

    DatabaseOperation {
        id: db_op
    }

    Column {

    spacing: 10
    anchors.centerIn: parent
    width: 300

    ListPickButton {
        id: listPicker
        textOnButton: "Choose the list to draw from"
        onClicked: {
            db_op.displayList(pickList,database,approachTactics.listKey)
            listPickDialog.visible=true
            listPickDialog.open()
        }
    }

    Dialog {
        id: listPickDialog
        width: 300
        height: 200
        autoSize: true
        isOpen: false
        positiveAction: false
        negativeAction: false
        title: "Choose the list to draw from"
        visible: false
        onClosed: {
            approachTactics.listChosen=true
            pickList.clear()
        }

        ListView {
            id: listPickList
            width: 280
            height: 140
            model: ListModel {
                id: pickList
            }
            delegate: ListPickButton {
                textOnButton: name
                onClicked: {
                    var currentItem=pickList.get(index)
                    listPicker.textOnButton=currentItem.name
                    openingLine.text=""
                    listPickDialog.visible=false
                    listPickDialog.close()
                }
            }
        }
    }


    AppText {
        id: openingLine
        width: parent.width
        wrapMode: "WordWrap"
    }

    MenuButton {
        textOnButton: "New Draw"
        onClicked: {
            if (approachTactics.listChosen) {
                db_op.getDBValue(database,listPicker.textOnButton,openingLine)
            }
            else {
                openingLine.text="Choose a list first"
                openingLine.color="red"
            }
        }
    }
}
}
