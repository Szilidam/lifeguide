import QtQuick 2.0
import Felgo 3.0

Rectangle {
    id: deleteButton
    width: 30
    height: 30

    property string defaultColor: "white"
    property string pressColor: "grey"
    property string textOnButton: "X"

    signal clicked


    radius: 4
    border.width: 2

    opacity: buttonArea.containsMouse ? 0.8:1
    color: buttonArea.pressed ? pressColor:defaultColor


    AppText {
        id: buttonText
        text: textOnButton
        anchors.centerIn: parent
        color: "red"
        font.bold: true
    }

    MouseArea {
        id: buttonArea
        hoverEnabled: true
        anchors.fill: deleteButton
        onClicked: deleteButton.clicked()
    }

}
