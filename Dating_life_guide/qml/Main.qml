import Felgo 3.0
import QtQuick 2.0
import "model"
import "logic"
import "pages"


App {
    id: app


    NavigationStack {
        id: navigator

        Page {
            id: mainMenu

            title: "Main Menu"

            Column {
                id: pageList
                spacing: app.dp(10)
                anchors.centerIn: parent

                MenuButton {
                    textOnButton: "Database"
                    onClicked: {
                        navigator.push(databaseManagement)
                    }
                }
                MenuButton {
                    textOnButton: "Draw"
                    onClicked: {
                        navigator.push(approachTactics)
                    }
                }

            }
        }

        Component {
            id: databaseManagement
        DatabaseManagementPage {

        }
        }

        Component {
            id: approachTactics
        ApproachTactics {

        }
        }

    }


}
